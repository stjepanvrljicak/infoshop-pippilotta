class AddExistsToBooks < ActiveRecord::Migration[5.1]
  def change
    add_column :books, :exists, :boolean
  end
end
