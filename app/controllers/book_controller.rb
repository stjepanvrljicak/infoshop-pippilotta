class BookController < ApplicationController
  def index
    @books = Book.all
    #TODO USER_ID
    @borrowed_books = Book.where(user_id: 1)
  end

  def show
    @book = Book.find(params[:id])

  end

  def borrow
    @book = Book.where(id: params[:book_id]).update_all(user_id: params[:user_id], date_borrowed: DateTime.now)
    redirect_to :action => "index"
  end

  def return
    @book = Book.where(id: params[:book_id]).update_all(user_id: nil, date_borrowed: nil)
    redirect_to action: "index"
  end
end
