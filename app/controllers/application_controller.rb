class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :authenticate

  private

  def authenticate
    authenticate_user! unless params[:controller] =~ /^admin\//
  end

  def set_admin_locale
    I18n.locale = :hr
  end


end
