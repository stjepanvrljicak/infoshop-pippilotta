ActiveAdmin.register User do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
permit_params :email, :first_name, :last_name
  menu priority: 5
  filter :first_name, label: "Ime"
  filter :last_name, label: "Prezime"
  actions :all, :except => :show

  index do
    column :email
    column 'Ime', :first_name
    column 'Prezime', :last_name
    actions
  end

  form do |f|
    f.inputs "Korisnicki podaci" do
      f.input :email
      f.input :first_name
      f.input :last_name
    end
  end
end
