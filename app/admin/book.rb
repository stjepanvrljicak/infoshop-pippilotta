ActiveAdmin.register Book do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params :id, :description, :author, :exists, :title, :place, :publisher, :year, :signature, :date_borrowed, :date_borrowed_date, :date_borrowed_time_hour, :date_borrowed_time_minute, :user_id
menu label: "Knjige", priority: 2
actions :all, :except => :show
filter :author
filter :year
filter :publisher
filter :date_borrowed
filter :title
filter :user_id, collection: proc { User.all }, as: :select, :label => 'Korisnik'
index do
  column "Postoji", :exists
  column "Naslov", :title
  column "Autor" do |c|
    if c.author == '/'
      '-'
    else
      c.author.to_s
    end
  end
  column "Opis", :description
  column "Godina izdanja", :year
  column "Izdavač", :publisher
  column "Posuđeno", :user
  actions
end

form do |f|
  f.inputs "Knjiga" do
    f.input :exists, :label => "Postoji"
    f.input :title, :label => "Naslov"
    f.input :author, :label => "Autor"
    f.input :year, :label => "Godina izdanja"
    f.input :publisher, :label => "Izdavač"
    f.input :place, :label => "Mjesto"
    f.input :date_borrowed, as: :just_datetime_picker, :label => "Datum posudbe"
    f.input :user
  end


  f.inputs "Opis" do
    f.input :description, :as => :text, :label => "Opis knjige"
  end

  f.submit "Spremi"
    controller do
      def show
        redirect_to admin_news_path
      end
    end
end

end
