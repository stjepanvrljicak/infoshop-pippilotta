class Book < ApplicationRecord
  just_define_datetime_picker :date_borrowed
  #validates :date_borrowed, :presence => true
  acts_as_paranoid
  belongs_to :user, optional: true
end
